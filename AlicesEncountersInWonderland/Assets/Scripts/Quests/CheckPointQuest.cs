﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointQuest : Quest
{
    //Reference to player position
    [SerializeField]
    private GameObject questObject;

    //Reference to position of goal
    [SerializeField]
    private GameObject goal;

    [SerializeField]
    private float distance = default; 

    //Overrides CheckCondition from Quest
    //Calls Success if distance is close enough to goal
    public override void CheckCondition()
    {
        float distance = Vector3.Distance(questObject.transform.position, goal.transform.position);

        if (distance <= this.distance)
        {
            OnSuccess();
        }
    }

}
