﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionQuest : Quest
{
    //Creates lit with object and number to collect
    [SerializeField]
    private List<GameObject> questItems; 

    //Checks if matching Item was found in inventory by name
    public override void CheckCondition()
    {
        foreach (GameObject i in questItems)
        {
            if (!Inventory.instance.HasItem(i.name))
            {
                return;
            }
        }

        OnSuccess();
        
        
    }

    public override void OnSuccess()
    {
        base.OnSuccess();
        
    }
}
