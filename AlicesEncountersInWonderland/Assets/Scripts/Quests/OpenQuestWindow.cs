﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class OpenQuestWindow : MonoBehaviour
{
    //Reference QuestWindowUIs
    public GameObject questWindowClosedUI;
    public GameObject questWindowOpenUI;

    //Deactivate and Activate Panel when key is pressed
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {

            if (questWindowClosedUI.activeSelf)
            {
                questWindowOpenUI.SetActive(true);
                questWindowClosedUI.SetActive(false);
            }
            else
            {
                questWindowOpenUI.SetActive(false);
                questWindowClosedUI.SetActive(true);
            }
        }
    }





}
