﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro; 
using UnityEngine.SceneManagement;

[System.Serializable]
public class Quest: MonoBehaviour
{
    
    public bool isActive; 

    //Quest description and title set in the inspector
    public string title;
    public string description;

    //References to next Quests or scenes
    public Quest sucsessNextQuest;
    public string sucsessNextScene;
    public Quest failureNextQuest;
    public string failureNextScene;

    //Reference to questWindow and its text
    public GameObject questWindowOpenUI;
    public TextMeshProUGUI questTitleText;
    public TextMeshProUGUI questDescriptionText;

    //Checks condition if Quest is running
    private void Update()
    {
        if (isActive)
        {
            CheckCondition();
            DisplayText();
        }
    }

    //Sets Avtice to & checks condition
    public void Activate()
    {
        isActive = true;
        Debug.Log("Quest activate " + this.title); 
        CheckCondition();
    }
    
    //meantto be overwritten
    public virtual void CheckCondition()
    {
        
    }

    //Disables Quest and activates next Scene or Quest if successful
    public virtual void OnSuccess()
    {
        isActive = false;

        Debug.Log("Quest success " + this.title);

        if (sucsessNextQuest)
        {
            sucsessNextQuest.Activate();
        }
        if (sucsessNextScene != "")
        {
            SceneManager.LoadScene(sucsessNextScene);
        }
    }

    //Disables Quest and activates next Scene or Quest on failure
    public virtual void OnFailure()
    {
        isActive = false;

        Debug.Log("Quest failure " + this.title);

        if (failureNextQuest)
        {
            failureNextQuest.Activate();
        }
        if (failureNextScene != "")
        {
            SceneManager.LoadScene(failureNextScene);
        }
    }

    //Display Text from Quest in QuestWindow
    public void DisplayText()
    {
        if (questWindowOpenUI != null)
        {
            questTitleText.text = title;
            questDescriptionText.text = description; 
        }  
    }
}
