﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    public GameObject[] popUps;
    private int popUpIndex;

    private void Update()
    {
        for (int i = 0; i < popUps.Length; i++)
        {
            if (i == popUpIndex)
                //making sure the popup disappears after it got noticed
            {
                popUps[popUpIndex].SetActive(true);
            }
            else
            {
                popUps[i].SetActive(false);
            }
        }

        if (popUpIndex == 0)
            //array starts at 0
        {
            if (Input.GetKeyDown(KeyCode.W))
                //first pop up appeares
            {
                popUpIndex++;
            }
        }
        else if (popUpIndex == 1)
            //if the first popup got played the next one appears, list goes on until every pop up got played
        {
                if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D))
                {
                    popUpIndex++;
                }
        }
        else if (popUpIndex == 2)
        {
                if (Input.GetKeyDown(KeyCode.LeftShift))
                {
                    popUpIndex++;
                }
        }
        else if (popUpIndex == 3)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                popUpIndex++;
            }
        }
        else if (popUpIndex == 4)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                popUpIndex++;
            }
        }
        else if (popUpIndex == 5)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                popUpIndex++;
            }
        }
        else if (popUpIndex == 6)
        {
            if (Input.GetKeyDown(KeyCode.I))
            {
                popUpIndex++;
            }
        }
        else if (popUpIndex == 7)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                popUpIndex++;
            }
        }
     
    }
}
