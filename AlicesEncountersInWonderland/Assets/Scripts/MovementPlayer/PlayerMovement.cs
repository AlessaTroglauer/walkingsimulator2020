﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //Reference to CharacterController
    [SerializeField]
    private CharacterController controller;

    //Speed of Movement 
    [SerializeField]
    private float moveSpeed;

    //Speed of Running Movement 
    [SerializeField]
    private float runSpeed;

    [SerializeField]
    private float gravity;

    [SerializeField]
    private float jumpHeight; 

    public Transform groundCheck;

    public float groundDistance;

    public LayerMask groundMask; 

    Vector3 velocity;
    bool isGrounded;

    public AudioSource walkAudioSource;

    public AudioClip walkingSound;




    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask); 
        
        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f; 
        }

        //Gather Input
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        //Turn Input into the direction the player should move and is facing
        Vector3 move = transform.right * x + transform.forward * z;

        //Move Player depending on speed and framerate independent;
        controller.Move(move * moveSpeed * Time.deltaTime);

        //Checking if the left shift Key is pressed and hold, if that's the case
        //Call move function with runSpeed
        if (Input.GetKey(KeyCode.LeftShift) )
        {
            controller.Move(move * runSpeed * Time.deltaTime);


        }

        //Moves player up if grounded and button press
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);


        }

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);

        
        //Play Sound when player is moving with keys
        if (Input.GetButtonDown("Horizontal") || Input.GetButtonDown("Vertical"))
        {
            if (!walkAudioSource.isPlaying)
            {
                walkAudioSource.Play();
            }
        }

        //Stop Playing Sound when Player stops pressing keys
        if (Input.GetButtonUp("Horizontal") || Input.GetButtonUp("Vertical"))
        {
            walkAudioSource.Stop();

        }
    }
}
