﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    //Speed of mouse
    [SerializeField]
    private float mouseSensitivity;

    //Reference from Camera to PLayerBody
    [SerializeField]
    private Transform playerBody; 

    //Rotation around x-Axis in order to move up and down
    private float xRotation = 0f;

    

    // Start is called before the first frame update
    void Start()
    {
        //Hide and lock cursor to center of screen
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

    }

    // Update is called once per frame
    void Update()
    {
        //Gather Input based on mouse movement
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        //Decrease xRotation based on mouseY
        //Clamping Rotation 
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        //Apply Rotation
        //Rotate PlayerBody around y-Axis
        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f); 
        playerBody.Rotate(Vector3.up * mouseX);


    }
}
