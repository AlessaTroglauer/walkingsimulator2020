﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSound : MonoBehaviour
{
    //Reference to the attached AudioSource component
    public AudioSource audioSource;

    //Value for Audioclip we want to play when hovering over
    public AudioClip hoverSound;

    //Value for Audioclip we want to play when clicking
    public AudioClip clickSound;

    //Method for sound Hovering set in in Event Trigger 
    //Plays assigned AudioClip once
    public void HoverSound()
    {
       audioSource.PlayOneShot(hoverSound);
    }

    //Method for sound Clicking set in in Event Trigger 
    //Plays assigned AudioClip once
    public void ClickSound()
    {
        audioSource.PlayOneShot(clickSound);
    }
}
