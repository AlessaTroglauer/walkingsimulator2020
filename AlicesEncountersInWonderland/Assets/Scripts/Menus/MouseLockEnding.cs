﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLockEnding : MonoBehaviour
{
    //unlocks mouse and makes it visible again
    private void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true; 
    }

    
}
