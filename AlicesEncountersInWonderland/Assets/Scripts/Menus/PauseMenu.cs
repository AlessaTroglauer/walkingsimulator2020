﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    //sets Game to active
    public static bool gameIsPaused = false;

    //Reference to pauseMenuUI
    public GameObject pauseMenuUI;


    ////Reference to the attached AudioSource component
    public AudioSource audioSource;

    //Value for Audioclip we want to play when opening Menu
    public AudioClip openPauseMenuSound;

    //Calling Methods when escape is pressed
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameIsPaused)
            {
                Resume();

            }
            else
            {
                Pause();
            }
        }
    }

    //Setting values for the buttons in the Pause Menu  
    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
        Screen.lockCursor = true;

    }

    //Setting values for calling the PauseMenu
    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
        audioSource.PlayOneShot(openPauseMenuSound);
        Screen.lockCursor = false;
    }

    //Method to LoadMainMenu
    public void LoadMenu()
    {
        Time.timeScale = 1f; 
        SceneManager.LoadScene(0);
    }

    //Method to Exit the Game
    public void QuitGame()
    {
        Application.Quit();
    }

}
