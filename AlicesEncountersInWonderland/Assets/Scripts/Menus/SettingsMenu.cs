﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio; 
using UnityEngine;

public class SettingsMenu : MonoBehaviour
{
    //reference to the attached AudioMixer
    public AudioMixer audioMixer; 

    //Gets called when mowing slider
    //Sets vakue of slider to volume
    public void SetVolume (float volume)
    {
        audioMixer.SetFloat("Volume", volume);
    }
}
