﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class MainMenu : MonoBehaviour
{
    //Function called from PlayButton
    //Load next Level 
    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    //Function called from QuitButton
    //Close Program
    public void Quitgame()
    {
        Application.Quit(); 
    }
}
