﻿using UnityEngine;

public class InventoryUI : MonoBehaviour
{

    public Transform itemsParent;
    public GameObject inventoryUI;
    public static bool gameIsPaused = false;

    //Reference to audio to play sound when opening
    public AudioSource audioSource;
    public AudioClip openInventoryUISound;

    public Inventory inventory;

    public GameObject pauseMenu; 

    InventorySlot[] slots;

    void Start()
    {
        inventory = Inventory.instance;
        inventory.onItemChangedCallback += UpdateUI;

        slots = itemsParent.GetComponentsInChildren<InventorySlot>();
    }


    void Update()
    {
        if (pauseMenu.activeSelf)
        {
            return;

        }

        if (Input.GetButtonDown("Inventory"))
        {
            if (gameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
            
            
        }
    }

    private void UpdateUI()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (i < inventory.items.Count)
            {
                slots[i].AddItem(inventory.items[i]);
            }
        }
    }

    //setting needed variables when Inventory is active
    void Pause()
    {
        inventoryUI.SetActive(true);
        gameIsPaused = true;
        audioSource.PlayOneShot(openInventoryUISound);
        Screen.lockCursor = false; 
    }

    //setting needed variables when Inventory is inactive
    public void Resume()
    {
        inventoryUI.SetActive(false);
        gameIsPaused = false;
        Screen.lockCursor = true;
    }


}
