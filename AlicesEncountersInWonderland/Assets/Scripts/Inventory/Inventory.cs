﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    #region Singleton

    public static Inventory instance;

    //make sure that only one inventory exists
    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instace of inventory found!");
            return;
        }

        instance = this;
    }

    #endregion


    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    public List<Item> items = new List<Item>();

    //Add item to inventory
    public void Add (Item item)
    {
        items.Add(item);

        Debug.Log("Adding item ti inventory " + item.name);

        if (onItemChangedCallback != null)
            onItemChangedCallback.Invoke();

    }

    //Check if need item for quest is in inventory
    public bool HasItem(string itemName)
    {
        Debug.Log("Item to find " + itemName); 

        foreach (Item i in items)
        {
            Debug.Log("item in list " + i.name);
            if (i.name == itemName)
            {
                return true;
            }
        }
        return false;
    }


}
