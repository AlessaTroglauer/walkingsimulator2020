﻿using UnityEngine;
using UnityEngine.UI; 

public class InventorySlot : MonoBehaviour
{
    //refernce to Image
    public Image icon;

    
    Item item; 

    //Adds Item to inventory
    public void AddItem (Item newItem)
    {
        item = newItem;

        icon.sprite = item.icon;
        icon.enabled = true; 
    }

    //Meant to be overwritten
    //Uses item if existent 
    public virtual void UseItem()
    {
        if (item != null)
        {
            item.Use();
        }
    }
}
