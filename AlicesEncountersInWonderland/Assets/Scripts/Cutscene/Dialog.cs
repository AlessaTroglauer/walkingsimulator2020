﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Dialog : MonoBehaviour
{
    public TextMeshProUGUI textDisplay;
    public string[] sentences;
    //choose the amount of sentences displayed in unity
    private int index;
    //what text should be displayed in unity
    public float typingSpeed;
    //speed of the text appearing

    public GameObject continueButton;
    //continue button
    public GameObject transitionButton;
    //scene transition button

    private void Start()
    {
        StartCoroutine(Type());
    }

    private void Update()
    {
        if (textDisplay.text == sentences[index])
            //checking if the displayed text is equal to the sentence which should be written
        {
            continueButton.SetActive(true);
            //enable continue button, once the sentence finished 
        }
    }

    IEnumerator Type()
    //typing effect
    {
        foreach (char letter in sentences[index].ToCharArray())
        {
            textDisplay.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }
    }

    public void NextSentence()
    //making the next sentence appear
    {
        continueButton.SetActive(false);
        //hide continue button 
        
        if (index < sentences.Length - 1)
        {
            index++;
            textDisplay.text = "";
            //making sure sentences dont stack up
            StartCoroutine(Type());
            //making sure the next sentence displays
        }
        else
        {
            textDisplay.text = "";
            //reset text after one sentence is complete
            continueButton.SetActive(false);
            //hide continue button, once the dialog ended
        }
        if(index>sentences.Length + 2)
        {
            transitionButton.SetActive(false);
        }
    }
}
