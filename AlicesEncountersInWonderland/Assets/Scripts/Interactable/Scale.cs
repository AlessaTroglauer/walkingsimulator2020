﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scale : MonoBehaviour
{
    //reference to player 
    public GameObject player;

    //Creating Vector3 
    public Vector3 scaleChange;  

    //Adds Vector3 to player transform scale
    public void Increase()
    {
        player.transform.localScale += scaleChange;
    }
}
