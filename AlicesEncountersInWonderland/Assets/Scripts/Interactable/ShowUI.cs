﻿using System.Collections;
using System.Collections.Generic;
using TMPro; 
using UnityEngine;

public class ShowUI : MonoBehaviour
{
    //Setting Rference to Panels and creating text 
    public GameObject dialogueUI;
    public string dialogue;
    public TextMeshProUGUI dialogueText;

    //disable dialogue window when starting game
    private void Start()
    {
        dialogueUI.SetActive(false);
    }

    //Display text when collidng with gameObject 
    private void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            dialogueUI.SetActive(true);
            dialogueText.text = dialogue; 
            StartCoroutine("WaitForSeconds");
        }
    }

    //Method to deactivate text after time 
    IEnumerator WaitForSeconds()
    {
        yield return new WaitForSeconds(5);
        dialogueUI.SetActive(false);
        Destroy(gameObject);
    }

}
