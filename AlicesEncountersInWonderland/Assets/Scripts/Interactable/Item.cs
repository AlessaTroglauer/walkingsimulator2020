﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Item : MonoBehaviour
{
    //Name and icon of Item set in the Inspector
    new public string name = "New Item";
    public Sprite icon = null;


    //reference panel
    public GameObject Panel; 

    //Activate panel when clicked on slot
    public void Use()
    {
        if(Panel != null)
        {
            Panel.SetActive(true);
            
        }
    }
     

}

