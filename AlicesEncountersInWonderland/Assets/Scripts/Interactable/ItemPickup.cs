﻿using UnityEngine;

public class ItemPickup : Interactable
{
    //Assigns Item
    public Item item; 

    //Override Interact method from interactable
    public override void Interact()
    {
        base.Interact();

        PickUp();
    }

    //Adds assigned Item to inventory and disables GameObject
    void PickUp()
    {
        Debug.Log("Picking up " + item.name);

        Inventory.instance.Add(item);

        
        gameObject.SetActive(false); 
    }
}
