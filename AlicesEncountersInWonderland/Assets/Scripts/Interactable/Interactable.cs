﻿using UnityEngine;
using System.Collections;
using TMPro;

public class Interactable : MonoBehaviour
{
    //Variables to check if close enough
    public float radius = default;
    public Transform player;
    public Transform interactionTransform;
    bool hasInteracted = false;

    //referneces for highlighting of mouse
    public GameObject mouseCursor;
    public GameObject highlightedmouseCursor;
    public bool mouseOver = false;

    //Refernces to UI and text to siplaay dialogue
    public GameObject dialogueUI;
    public string dialogue;
    public TextMeshProUGUI dialogueText;
    public float timetoAppear;
    private float timeWhenDisappear;

    //Reference to audioSource and clip
    public AudioSource audioSource;
    public AudioClip interactionSound;



    //Interacting method
    public virtual void Interact()
    {
        //This method is meant to be overwritten
        Debug.Log("Interacting with" + transform.name);
    }

    //Interaction, displaying text and playing audio source if closeenough, mouse click & hovering over 
    private void Update()
    {
        float distance = Vector3.Distance(player.position, interactionTransform.position);

        if (!hasInteracted && Input.GetMouseButtonDown(0) && mouseOver)
        {
            if (distance <= radius)
            {

                audioSource.PlayOneShot(interactionSound);
                Interact();
                hasInteracted = true;

                mouseOver = false;
                highlightedmouseCursor.SetActive(false);
                mouseCursor.SetActive(true);


                if (player.gameObject.tag == "Player")
                {
                    dialogueUI.SetActive(true);
                    dialogueText.text = dialogue;
                    Invoke("showDialogue", 4);
                }
            }

        }



    }
    void showDialogue()
    {
        dialogueUI.SetActive(false);
    }





   //creates sphere to set interaction rasius from transform of object
    private void OnDrawGizmosSelected()
    {
        if (interactionTransform == null) 
            interactionTransform = transform; 

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(interactionTransform.position, radius);
    }

    //changing material when hovering over
    void OnMouseEnter()
    {
        mouseOver = true;
        highlightedmouseCursor.SetActive(true);
        mouseCursor.SetActive(false);
    }

    //changing material back to its original when leaving
    void OnMouseExit()
    {
        mouseOver = false;
        highlightedmouseCursor.SetActive(false);
        mouseCursor.SetActive(true);
    }


}
